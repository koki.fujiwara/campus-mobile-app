import React, { Component } from "react";
import {
	ScrollView,
	StyleSheet,
	Text,
	FlatList,
	TouchableWithoutFeedback,
	TouchableOpacity,
	Platform
} from "react-native";
import { ListItem, Divider } from "react-native-elements";
import Colors from "../constants/Colors";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import { fetchGroups } from "../redux/ActionCreators";

const mapStateToProps = state => {
	return {
		users: state.users
	};
};

const mapDispatchToProps = dispatch => ({
	fetchUsers: userOffset => dispatch(fetchUsers(userOffset)),
	fetchFilterUsers: (users, keywords, userOffset) => dispatch(fetchFilterUsers(users, keywords, userOffset))
});

class GroupScreen extends Component {
	render() {
		return (
			<>
				<ScrollView style={styles.container}></ScrollView>
				<TouchableOpacity style={styles.fab}>
					<Ionicons name={Platform.OS === "ios" ? `ios-create` : "md-create"} size={26} style={styles.text} />
				</TouchableOpacity>
			</>
		);
	}
}

GroupScreen.navigationOptions = {
	title: "Group",
	header: null
	// headerStyle: {
	// 	backgroundColor: Colors.tintColor
	// },
	// headerTintColor: "#fff",
	// headerTitleStyle: {
	// 	color: "#fff"
	// }
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	},
	fab: {
		height: 50,
		width: 50,
		borderRadius: 200,
		position: "absolute",
		bottom: 20,
		right: 20,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: Colors.accentColor,
		elevation: 8
	},
	text: {
		fontSize: 30,
		color: "white"
	}
});

export default GroupScreen;
