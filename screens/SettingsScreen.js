import React, { Component } from "react";
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, FlatList } from "react-native";
import { ListItem, Divider, Button } from "react-native-elements";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import { logout } from "../redux/ActionCreators";

const mapStateToProps = state => {
	return { login: state.login };
};

const mapDispatchToProps = dispatch => ({
	logout: () => dispatch(logout())
});

const settings = [
	{ id: 0, key: "account", desc: "My Account", dest: "AccountInfo" },
	{ id: 1, key: "appInfo", desc: "App Info", dest: "AppInfo" }
];

class SettingsScreen extends Component {
	render() {
		const { navigate } = this.props.navigation;

		const RenderSettings = ({ item, index }) => {
			return (
				<>
					<TouchableOpacity onPress={() => navigate(item.dest)}>
						<ListItem
							key={index}
							title={item.desc}
							style={{ fontSize: 20 }}
							hideChevron={true}
							style={{ paddingVertical: 5, marginVertical: 5 }}
						/>
					</TouchableOpacity>
					<Divider />
				</>
			);
		};

		return (
			<ScrollView style={styles.container}>
				<Text style={styles.developmentModeText}>This is settings Screen</Text>
				<Divider />
				<FlatList
					data={settings}
					style={{ marginBottom: 20 }}
					renderItem={RenderSettings}
					keyExtractor={item => item.id.toString()}
				/>
				<View style={styles.delButton}>
					<Button title="Log Out" raised={true} onPress={() => this.props.logout()} color="red" />
				</View>
			</ScrollView>
		);
	}
}

SettingsScreen.navigationOptions = {
	title: "Setting",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(SettingsScreen);
