import React, { Component } from "react";
import {
	View,
	ScrollView,
	StyleSheet,
	Text,
	FlatList,
	TouchableWithoutFeedback,
	TouchableOpacity,
	Platform,
	Modal,
	TextInput
} from "react-native";

import { ListItem, Divider, SearchBar } from "react-native-elements";
import { MESSAGES } from "../shared/messages";
import Colors from "../constants/Colors";
import { Ionicons } from "@expo/vector-icons";
import { connect } from "react-redux";
import { fetchUsers, fetchFilterUsers } from "../redux/ActionCreators";

const mapStateToProps = state => {
	return {
		users: state.users
	};
};

const mapDispatchToProps = dispatch => ({
	fetchUsers: userOffset => dispatch(fetchUsers(userOffset)),
	fetchFilterUsers: (users, keywords, userOffset) => dispatch(fetchFilterUsers(users, keywords, userOffset))
});

class ChatScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			messages: MESSAGES,
			userOffset: 1,
			modalVisible: false,
			userFilter: ""
		};
		this.filterUser = this.filterUser.bind(this);
		this.scrollToBottom = this.scrollToBottom.bind(this);
	}

	async componentDidMount() {
		await this.props.fetchUsers(this.state.userOffset);
	}

	async loadUsers(offset) {
		this.props.fetchUsers(offset);
	}

	async filterUser(text) {
		this.setState({ userFilter: text });
		// this.props.fetchFilterUsers(this.props.users.users, text, this.state.userOffset);
	}

	isCloseToBottom({ layoutMeasurement, contentOffset, contentSize }) {
		return layoutMeasurement.height + contentOffset.y >= contentSize.height - 50;
	}

	scrollToBottom({ layoutMeasurement, contentOffset, contentSize }) {
		this.scrollView.scrollTo({ y: contentOffset.y });
	}

	render() {
		const { navigate } = this.props.navigation;

		const RenderModal = () => {
			const RenderUser = ({ item, index }) => {
				return (
					<>
						<TouchableWithoutFeedback>
							<ListItem
								key={index}
								title={item.last_name + " " + item.first_name}
								subtitle={"@" + item.username}
								hideChevron={true}
								leftAvatar={{ source: require("../assets/images/services/nurse01.jpg") }}
							/>
						</TouchableWithoutFeedback>
						<Divider />
					</>
				);
			};

			return (
				<Modal animationType="slide" transparent={true} visible={true}>
					<View style={styles.modalContainer}>
						<View style={styles.modalHeader}>
							<Text style={styles.headerTitle}>New Conversation</Text>

							<TouchableOpacity style={styles.closeButton}>
								<Ionicons
									onPress={() => {
										this.setState({ modalVisible: false });
									}}
									name={Platform.OS === "ios" ? `ios-close-circle` : "md-close-circle"}
									size={34}
								/>
							</TouchableOpacity>

							<View style={{ backgroundColor: "white", width: "100%", height: "15%", borderRadius: 8 }}>
								<Divider />
								<TextInput
									onChangeText={text => this.setState({ userFilter: text })}
									// placeholder={"Search"}
									// containerStyle={{ backgroundColor: "white" }}
									// inputContainerStyle={{ backgroundColor: "white" }}
									value={this.state.userFilter}
								/>
							</View>

							<View style={{ backgroundColor: "white", width: "100%", height: "77%" }}>
								<ScrollView
									ref={ref => {
										this.scrollView = ref;
									}}
									onScroll={({ nativeEvent }) => {
										if (this.isCloseToBottom(nativeEvent)) {
											this.scrollToBottom(nativeEvent);
											this.setState({ userOffset: this.state.userOffset + 1 });
											this.loadUsers(this.state.userOffset);
										}
									}}
								>
									<FlatList
										data={this.props.users.users}
										renderItem={RenderUser}
										keyExtractor={item => item.id.toString()}
									/>
								</ScrollView>
							</View>
						</View>
					</View>
				</Modal>
			);
		};

		const RenderMessages = ({ item, index }) => {
			return (
				<>
					<TouchableWithoutFeedback onPress={() => navigate("ChatRoom", { chatRoom: item })}>
						<ListItem
							key={index}
							title={item.username}
							subtitle={item.lastMessage}
							hideChevron={true}
							leftAvatar={{ source: require("../assets/images/services/nurse01.jpg") }}
						/>
					</TouchableWithoutFeedback>
					<Divider />
				</>
			);
		};

		return (
			<>
				{this.state.modalVisible && <RenderModal />}
				<ScrollView style={styles.container}>
					<Text style={styles.developmentModeText}>This is Chat Overview Screen</Text>
					<FlatList
						data={this.state.messages}
						renderItem={RenderMessages}
						keyExtractor={item => item.chatRoomId.toString()}
					/>
				</ScrollView>
				<TouchableOpacity style={styles.fab}>
					<Ionicons
						name={Platform.OS === "ios" ? `ios-create` : "md-create"}
						size={26}
						style={styles.text}
						onPress={() => this.setState({ modalVisible: !this.state.modalVisible })}
					/>
				</TouchableOpacity>
			</>
		);
	}
}

ChatScreen.navigationOptions = {
	title: "Chat",
	header: null
	// headerStyle: {
	// 	backgroundColor: Colors.tintColor
	// },
	// headerTintColor: "#fff",
	// headerTitleStyle: {
	// 	color: "#fff"
	// }
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	},
	fab: {
		height: 50,
		width: 50,
		borderRadius: 200,
		position: "absolute",
		bottom: 20,
		right: 20,
		justifyContent: "center",
		alignItems: "center",
		backgroundColor: Colors.accentColor,
		elevation: 8
	},
	modalContainer: {
		borderRadius: 12,
		width: "95%",
		height: "95%",
		position: "absolute",
		left: "2.5%",
		top: "2.5%",
		padding: 20,
		backgroundColor: "white",
		elevation: 8
	},
	modalHeader: {
		flex: 1,
		flexDirection: "row",
		flexWrap: "wrap"
		// backgroundColor: "red"
	},
	headerTitle: {
		fontSize: 20,
		// fontStyle: "bold",
		width: "90%",
		height: "8%"
		// backgroundColor: "blue"
	},
	closeButton: {
		width: "10%",
		height: "8%"
		// backgroundColor: "green"
	},
	text: {
		fontSize: 30,
		color: "white"
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ChatScreen);
