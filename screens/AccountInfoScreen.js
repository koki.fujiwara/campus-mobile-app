import React, { Component } from "react";
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, FlatList } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { ListItem, List, Button } from "react-native-elements";
import { USER } from "../shared/user";
import Colors from "../constants/Colors";
import { connect } from "react-redux";
import { logout } from "../redux/ActionCreators";

const mapStateToProps = state => {
	return {
		user: state.login
	};
};

const mapDispatchToProps = dispatch => ({
	logout: () => dispatch(logout())
});

class AccountInfoScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			user: USER
		};
	}

	render() {
		const user = this.state.user;

		return (
			<ScrollView style={styles.container}>
				<Text style={styles.developmentModeText}>This is settings Screen</Text>

				<View style={styles.fieldContainer}>
					<Text key="firstName" style={styles.descriptionText}>
						First Name
					</Text>
					<Text style={styles.contentText}>{user.firstName}</Text>
				</View>

				<View style={styles.fieldContainer}>
					<Text key="lastName" style={styles.descriptionText}>
						First Name
					</Text>
					<Text style={styles.contentText}>{user.lastName}</Text>
				</View>
				<View style={styles.fieldContainer}>
					<Text key="userLevel" style={styles.descriptionText}>
						User Type
					</Text>
					<Text style={styles.contentText}>{user.userLevel}</Text>
				</View>
				<View style={styles.fieldContainer}>
					<Text key="username" style={styles.descriptionText}>
						Username
					</Text>
					<Text style={styles.contentText}>{this.props.user.username}</Text>
				</View>
				<View style={styles.fieldContainer}>
					<Text key="wechatId" style={styles.descriptionText}>
						weChat Id
					</Text>
					<Text style={styles.contentText}>{user.wechatId}</Text>
				</View>
				<View style={styles.delButton}>
					<Button title="Log Out" raised={true} onPress={() => this.props.logout()} />
				</View>
				<View style={{ marginTop: 20 }} />
				<View style={styles.delButton}>
					<Button
						title="Delete My Account"
						raised={true}
						buttonStyle={{ backgroundColor: "red" }}
						onPress={() => alert("Are you sure?")}
					/>
				</View>
			</ScrollView>
		);
	}
}

AccountInfoScreen.navigationOptions = {
	title: "My Account",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	},
	headerRight: (
		<TouchableOpacity onPress={() => alert("You can edit!")}>
			<Ionicons
				name={Platform.OS === "ios" ? `ios-create` : "md-create"}
				size={26}
				style={{ marginRight: 16 }}
				color="#fff"
			/>
		</TouchableOpacity>
	)
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	},
	descriptionText: {
		marginLeft: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19
		// textAlign: "center"
	},
	contentText: {
		marginLeft: 20,
		fontSize: 26
	},
	delButton: {
		marginVertical: 40,
		marginHorizontal: 20
	},
	fieldContainer: {
		height: 50,
		marginVertical: 10
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AccountInfoScreen);
