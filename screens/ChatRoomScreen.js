import React, { Component } from "react";
import {
	ScrollView,
	View,
	StyleSheet,
	Text,
	FlatList,
	TextInput,
	KeyboardAvoidingView,
	TouchableOpacity
} from "react-native";
import { ListItem, Button, Input, Divider } from "react-native-elements";
import { MESSAGES } from "../shared/messages";
import Colors from "../constants/Colors";

class ChatRoomScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			messages: MESSAGES
		};
	}

	static navigationOptions = {
		title: "Chat Room",
		headerStyle: {
			backgroundColor: Colors.tintColor
		},
		headerTintColor: "#fff",
		headerTitleStyle: {
			color: "#fff"
		}
	};

	render() {
		// const roomId = this.props.navigation.getParam("chatRoomId", "");

		// const thisMessage = this.state.messages.filter(message => message.chatRoomId == roomId)[0];
		const thisMessage = this.props.navigation.getParam("chatRoom", "");

		this.navigationOptions = { title: thisMessage.username };

		const RenderMessage = ({ item, index }) => {
			return (
				<View style={{ flex: 1, flexDirection: "column" }}>
					<View style={item.isUser ? styles.chatUser : styles.chat}>
						<Text style={item.isUser ? styles.textUser : null}>{item.message}</Text>
					</View>
				</View>
			);
		};

		return (
			<>
				<ScrollView style={styles.container}>
					<Text style={styles.developmentModeText}>This is Chat Room Screen</Text>
					<FlatList
						data={thisMessage.content}
						renderItem={RenderMessage}
						keyExtractor={item => item.messageId.toString()}
					/>
				</ScrollView>
				<KeyboardAvoidingView style={{ position: "absolute", left: 0, right: 0, bottom: 0 }}>
					<Divider />
					<TextInput
						style={styles.newInput}
						value={this.state.newMessage}
						onSubmitEditing={this.sendMessage}
						placeholder="Message..."
						onChangeText={this.updateMessageState}
					/>
				</KeyboardAvoidingView>
			</>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	},
	chat: {
		borderWidth: 0.5,
		borderRadius: 4,
		marginVertical: 5,
		marginLeft: 10,
		paddingVertical: 10,
		paddingHorizontal: 15,
		marginRight: 120
	},
	chatUser: {
		borderRadius: 4,
		marginVertical: 5,
		marginLeft: 120,
		paddingVertical: 10,
		paddingHorizontal: 15,
		marginRight: 10,
		backgroundColor: Colors.tintColor
	},
	textUser: {
		color: "#fff"
	},
	newInput: {
		borderWidth: 1,
		borderColor: "#ccc",
		fontSize: 16,
		padding: 10,
		height: 50
	}
});

export default ChatRoomScreen;
