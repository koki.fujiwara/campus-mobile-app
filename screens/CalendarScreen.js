import React from "react";
import { ScrollView, StyleSheet, Text, View } from "react-native";
import { Calendar } from "react-native-calendars";
import Colors from "../constants/Colors";

function CalendarScreen() {
	return (
		<View>
			<Calendar current={"2019-08-16"} monthFormat={"yyyy MMM"} firstDay={1} />
		</View>
	);
}

CalendarScreen.navigationOptions = {
	title: "Calendar",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	}
});

export default CalendarScreen;
