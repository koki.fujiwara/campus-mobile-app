import React from "react";
import { ExpoConfigView } from "@expo/samples";
import Colors from "../constants/Colors";

function AppInfoScreen() {
	return <ExpoConfigView />;
}

AppInfoScreen.navigationOptions = {
	title: "App Info",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

export default AppInfoScreen;
