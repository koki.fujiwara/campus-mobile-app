import React, { Component } from "react";
import LoginScreen from "./registry/LoginScreen";
import AppNavigator from "../navigation/AppNavigator";
import { connect } from "react-redux";

const mapStateToProps = state => {
	return { login: state.login };
};

class MainScreen extends Component {
	render() {
		if (this.props.login.loggedIn) {
			return <AppNavigator />;
		} else {
			return <LoginScreen />;
		}
	}
}

export default connect(mapStateToProps)(MainScreen);
