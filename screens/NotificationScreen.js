import React, { Component } from "react";
import { Image, IconBadge, Platform, Badge, ScrollView, StyleSheet, Text, TouchableOpacity, View, FlatList } from "react-native";
import { ListItem, Divider } from "react-native-elements";
import { NOTIFICATONS } from "../shared/notifications";
import Colors from "../constants/Colors";


class NotificationScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			notifications: NOTIFICATONS,
		};
	}

	render() {
		const { navigate } = this.props.navigation;

		const RenderNotifications = ({ item, index }) => {

			return (

				<ListItem
					key={index}
					title={
						<View>
							<Text style={item.read ? styles.textNormaltitle : styles.textBoldtitle}>{item.username}</Text>
						</View>
					}
					subtitle={
						<View>
							<Text style={item.read ? styles.textNormalsub : styles.textBoldsub}>{item.notificationtype}</Text>
							<Text style={item.read ? styles.textNormalsub : styles.textBoldsub}>{item.notificationdate + " " + item.notificationtime}</Text>
						</View>
					}
					leftAvatar={{ source: require("../assets/images/services/nurse01.jpg") }}
					badge={item.read ? null : { status: item.important ? "error" : "primary" }}
					onPress={() => navigate(item.navigate)}
				/>
			);
		};

		return (
			<ScrollView style={styles.container}>
				<FlatList data={this.state.notifications} renderItem={RenderNotifications} keyExtractor={item => item.id.toString()} />
			</ScrollView>
		);
	}
}

NotificationScreen.navigationOptions = {
	title: "Notification",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},

	textNormaltitle: {
		fontWeight: "normal",
		fontSize: 16
	},

	textNormalsub: {
		fontWeight: "normal",
		fontSize: 12
	},

	textBoldtitle: {
		fontWeight: "bold",
		fontSize: 16
	},

	textBoldsub: {
		fontWeight: "bold",
		fontSize: 12
	}
});

export default NotificationScreen;
