import React, { Component } from "react";
import { Text, TextInput, View, Button, StyleSheet, Image } from "react-native";
import { connect } from "react-redux";
import { userLogin } from "../../redux/ActionCreators";
// import FBLoginButton from "./thirdPartyLogin/FBLoginButton";

const mapStateToProps = state => {
	return { login: state.login };
};

const mapDispatchToProps = dispatch => ({
	userLogin: login_info => dispatch(userLogin(login_info))
});

hashCode = function(s) {
	var h = 0,
		l = s.length,
		i = 0;
	if (l > 0) while (i < l) h = ((h << 5) - h + s.charCodeAt(i++)) | 0;
	return h;
};

class LoginScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			loginInfo: {
				username: "",
				pass_hash: null
			}
		};
		this.hashPassword = this.hashPassword.bind(this);
	}

	hashPassword = password => {
		pass = hashCode(password);
		this.setState(prevState => ({ loginInfo: { ...prevState.loginInfo, pass_hash: pass } }));
	};

	render() {
		return (
			<View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
				<View>
					<Image
						source={require("../../assets/images/logos/campus-logo-blue-title.png")}
						style={{
							width: 300,
							height: 150,
							margin: 30,
							resizeMode: "contain"
						}}
					/>
				</View>
				<View style={{ width: "80%", justifyContent: "center", alignItems: "center" }}>
					<TextInput
						placeholder="Username"
						style={styles.input}
						onChangeText={text =>
							this.setState(prevState => ({ loginInfo: { ...prevState.loginInfo, username: text } }))
						}
					/>
					<TextInput
						placeholder="Password"
						style={styles.input}
						secureTextEntry={true}
						onChangeText={text => this.hashPassword(text)}
					/>
					<View style={{ margin: 7 }} />
					<Button style={{ fontSize: 28 }} onPress={() => this.props.userLogin(this.state.loginInfo)} title="Login" />
					<View style={{ margin: 7 }} />
					{/* <FBLoginButton /> */}
				</View>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	input: {
		margin: 10,
		paddingVertical: 5,
		paddingHorizontal: 12,
		fontSize: 18,
		borderWidth: 0.5,
		borderRadius: 8,
		width: "100%"
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginScreen);
