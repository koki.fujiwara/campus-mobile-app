import React, { Component } from "react";
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, FlatList } from "react-native";
import { ListItem, Card } from "react-native-elements";
import { connect } from "react-redux";
import { fetchSelectedCourse } from "../../redux/ActionCreators";

import Colors from "../../constants/Colors";

const mapStateToProps = state => {
	return {
		selectedCourse: state.selectedCourse
	};
};

const mapDispatchToProps = dispatch => ({
	fetchSelectedCourse: courseId => dispatch(fetchSelectedCourse(courseId))
});

function minToTime(totalMinutes) {
	var h = totalMinutes % 60;
	var m = totalMinutes - h * 60;

	retTime = h > 0 ? h.toString() + "h " : "" + m == NaN ? "0" : m.toString() + "m";

	return retTime;
}

class ModuleScreen extends Component {
	async componentDidMount() {
		const courseId = this.props.navigation.getParam("courseId", "");
		await this.props.fetchSelectedCourse(courseId);
	}

	render() {
		const { navigate } = this.props.navigation;

		const RenderCourseHeader = ({ course }) => {
			if (course != null) {
				return (
					<Card
						featuredTitle={course.title}
						featuredSubtitle={minToTime(course.total_time) + " remaining"}
						image={require("../../assets/images/services/nurse01.jpg")}
					>
						<Text style={{ margin: 5 }}>{course.description}</Text>
					</Card>
				);
			} else {
				return <View />;
			}
		};

		const RenderModules = ({ item, index }) => {
			if (item != null) {
				return (
					<ListItem
						key={index}
						title={item.title}
						subtitle={minToTime(item.total_time) + " remaining"}
						hideChevron={true}
						onPress={() => navigate("Item", { moduleId: index })}
						leftAvatar={{ source: require("../../assets/images/services/nurse01.jpg") }}
					/>
				);
			} else {
				return <View />;
			}
		};

		return (
			<ScrollView style={styles.container}>
				<Text style={styles.developmentModeText}>This is Module Overview Screen</Text>
				<RenderCourseHeader course={this.props.selectedCourse.course} />
				<Text style={{ fontSize: 22, alignSelf: "center" }}>Modules</Text>
				<FlatList
					data={this.props.selectedCourse.course ? this.props.selectedCourse.course.modules : null}
					renderItem={RenderModules}
					keyExtractor={item => item.id.toString()}
				/>
			</ScrollView>
		);
	}
}

ModuleScreen.navigationOptions = {
	title: "Module",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ModuleScreen);
