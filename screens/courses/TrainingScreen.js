import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import QRCode from "react-native-qrcode";
import { Button } from "react-native-elements";
import { connect } from "react-redux";
import io from "socket.io-client";

import Colors from "../../constants/Colors";
import { baseUrl } from "../../shared/baseUrl";
import { connectSocket, reconnectSocket } from "../../redux/ActionCreators";

const mapStateToProps = state => {
	return {
		sockets: state.sockets
	};
};

const mapDispatchToProps = dispatch => ({
	connectSocket: (socket, status) => dispatch(connectSocket(socket, status)),
	reconnectSocket: socket => dispatch(reconnectSocket(socket))
});

class TrainingScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			itemId: ""
		};
		this.newSession = this.newSession.bind(this);
	}

	async componentWillMount() {
		const itemId = this.props.navigation.getParam("itemId", "");
		this.setState({ itemId: itemId });

		this.socket = new io(baseUrl);

		this.props.reconnectSocket(this.socket);

		// if (this.props.sockets.status.sessionId === null) {
		// 	alert("no session ID");
		// }
	}

	componentWillUnmount() {
		this.socket.emit("disconnect");
	}

	newSession() {
		console.log("Starting a new session");
		this.socket = new io(baseUrl);
		this.props.reconnectSocket(this.socket);
	}

	render() {
		const RenderQRCode = () => {
			const value = {
				...this.props.sockets.status,
				userId: 23,
				itemId: this.state.itemId
			};

			return (
				<View>
					<QRCode
						style={{ marginTop: 100, opacity: 0.5 }}
						value={JSON.stringify(value)}
						size={250}
						bgColor="#000"
						fgColor="#fff"
					/>
				</View>
			);
		};

		const RenderContent = () => {
			// if (this.props.sockets.status.sessionStarted) {
			if (this.props.sockets.status == null) {
				return <RenderQRCode />;
			} else if (this.props.sockets.status.sessionStarted) {
				// if (this.props.sockets.status.recordStarted) {
				// 	return <Button title={"Stop"} onPress={() => this.socket.emit("stopRecord")} />;
				// } else {
				return (
					<>
						<Button
							title={"Record"}
							style={{ width: 100, margin: 50 }}
							onPress={() => this.socket.emit("startRecord")}
						/>
						<Button title={"Stop"} style={{ width: 100, margin: 50 }} onPress={() => this.socket.emit("stopRecord")} />
					</>
				);
				// }
			} else {
				return <RenderQRCode />;
			}
			// } else {
			// 	return <></>;
			// }
		};

		return (
			<>
				{/* <Text style={styles.developmentModeText}>This is Training Screen</Text>
				<Text style={styles.developmentModeText}>Item ID: {this.state.itemId.toString()}</Text> */}

				<View style={styles.container}>
					<RenderContent />
				</View>
				<View>
					<Button title={"New Session"} style={{ width: 100, margin: 50 }} onPress={() => this.newSession()} />
					<Text style={{ textAlign: "left" }}>{JSON.stringify(this.props.sockets)}</Text>
				</View>
			</>
		);
	}
}

TrainingScreen.navigationOptions = {
	title: "Training",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff",
		justifyContent: "center",
		alignItems: "center"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	},
	icon: { width: 30, alignContent: "center" }
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TrainingScreen);
