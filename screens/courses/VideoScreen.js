import React, { Component } from "react";
import { View, StyleSheet, Text, Dimensions, TouchableWithoutFeedback } from "react-native";
import { Video } from "expo-av";
import Colors from "../../constants/Colors";
import { MaterialIcons, Octicons } from "@expo/vector-icons";

class VideoScreen extends Component {
	constructor(props) {
		super(props);

		const screenWidth = Math.round(Dimensions.get("window").width);

		this.state = {
			screenWidth: screenWidth,
			heightScaled: 300,
			mute: false,
			shouldPlay: true,
			controlVisible: true
		};

		// this.handlePlayAndPause = this.handlePlayAndPause.bind(this);
		// this.handleVolume = this.handleVolume.bind(this);
		// this.handleControl = this.handleControl.bind(this);
	}

	componentWillUnmount() {
		this.setState({ shouldPlay: false });
	}

	handlePlayAndPause = () => {
		this.setState(prevState => ({
			shouldPlay: !prevState.shouldPlay,
			controlVisible: this.state.shouldPlay
		}));
	};

	handleVolume = () => {
		this.setState(prevState => ({
			mute: !prevState.mute
		}));
	};

	handleControl = () => {
		if (!this.state.shouldPlay) {
			this.setState({ controlVisible: true });
		} else {
			this.setState(prevState => ({
				controlVisible: !prevState.controlVisible
			}));
		}
	};

	render() {
		const RenderControl = () => {
			if (this.state.controlVisible) {
				return (
					<View style={styles.controlBar}>
						<MaterialIcons
							name={this.state.mute ? "volume-mute" : "volume-up"}
							size={45}
							color="white"
							onPress={this.handleVolume}
						/>
						<MaterialIcons
							name={this.state.shouldPlay ? "pause" : "play-arrow"}
							size={45}
							color="white"
							onPress={this.handlePlayAndPause}
						/>
					</View>
				);
			} else {
				return null;
			}
		};

		return (
			<>
				<TouchableWithoutFeedback onPress={this.handleControl}>
					<View>
						<Video
							source={{ uri: "http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4" }}
							shouldPlay={this.state.shouldPlay}
							isMuted={this.state.mute}
							resizeMode="contain"
							style={{ width: this.state.screenWidth, height: this.state.heightScaled }}
							onReadyForDisplay={response => {
								const { width, height } = response.naturalSize;
								const heightScaled = height * (this.state.screenWidth / width);
								this.setState({
									heightScaled: heightScaled
								});
							}}
						/>
						<RenderControl />
					</View>
				</TouchableWithoutFeedback>
			</>
		);
	}
}

VideoScreen.navigationOptions = {
	title: "Video",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	controlBar: {
		position: "absolute",
		bottom: 0,
		left: 0,
		right: 0,
		top: 0,
		// height: 50,
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "rgba(0, 0, 0, 0.5)"
	}
});

export default VideoScreen;
