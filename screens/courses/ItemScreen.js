import React, { Component } from "react";
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableOpacity, View, FlatList } from "react-native";
import { ListItem, Button } from "react-native-elements";
import { Ionicons } from "@expo/vector-icons";
import Colors from "../../constants/Colors";
import { fetchItems } from "../../redux/ActionCreators";
import { connect } from "react-redux";

const mapStateToProps = state => {
	return { items: state.items };
};

const mapDispatchToProps = dispatch => ({
	fetchItems: moduleId => dispatch(fetchItems(moduleId))
});

class ItemScreen extends Component {
	async componentDidMount() {
		const moduleId = this.props.navigation.getParam("moduleId", "");
		await this.props.fetchItems(moduleId);
	}

	render() {
		const { navigate } = this.props.navigation;

		function ItemIcon(item) {
			// console.log(item.is_video);

			if (item.is_video === "true") {
				retIcon = Platform.OS === "ios" ? `ios-play` : "md-play";
			} else if (item.is_homework === "true") {
				retIcon = Platform.OS === "ios" ? `ios-list-box` : "md-list-box";
			} else if (item.is_training === "true") {
				retIcon = Platform.OS === "ios" ? `ios-videocam` : "md-videocam";
			} else {
				retIcon = Platform.OS === "ios" ? `ios-help` : "md-help";
			}

			return retIcon;
		}

		function navigateItem(item, index) {
			if (item.is_video === "true") {
				navigate("Video");
			} else if (item.is_homework === "true") {
				navigate("Video");
			} else if (item.is_training === "true") {
				navigate("Training", { itemId: index });
			}
		}

		const RenderItem = ({ item, index }) => {
			return (
				<TouchableOpacity style={{ marginVertical: 3, marginHorizontal: 10 }} onPress={() => navigateItem(item, index)}>
					{/* <Ionicons name={ItemIcon(item.itemType)} size={26} style={styles.icon} /> */}
					{/* <Text>{item.title}</Text> */}

					<Button
						key={index}
						title={item.title}
						icon={<Ionicons name={ItemIcon(item)} size={26} style={styles.icon} />}
						buttonStyle={{ paddingVertical: 15, paddingHorizontal: 15, borderColor: "#fff", alignSelf: "flex-start" }}
						titleStyle={{
							color: "#000",
							textAlign: "left",
							marginLeft: 10
						}}
						raised={true}
						type="outline"
						onPress={() => navigateItem(item, index)}
					/>
				</TouchableOpacity>
			);
		};

		return (
			<ScrollView style={styles.container}>
				<Text style={styles.developmentModeText}>This is Item Overview Screen</Text>
				<Text style={styles.developmentModeText}>{this.props.items.errMess}</Text>
				<FlatList data={this.props.items.items} renderItem={RenderItem} keyExtractor={item => item.id.toString()} />
			</ScrollView>
		);
	}
}

ItemScreen.navigationOptions = {
	title: "Items",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	},
	icon: { width: 30, alignContent: "center" }
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(ItemScreen);
