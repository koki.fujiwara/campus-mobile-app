import React, { Component } from "react";
import { Image, Platform, ScrollView, StyleSheet, Text, TouchableWithoutFeedback, View, FlatList } from "react-native";
import { Card, Divider, Button } from "react-native-elements";
import { connect } from "react-redux";
import { fetchCourses } from "../../redux/ActionCreators";

import Colors from "../../constants/Colors";

const mapStateToProps = state => {
	return {
		courses: state.courses
	};
};

const mapDispatchToProps = dispatch => ({
	fetchCourses: () => dispatch(fetchCourses())
});

function minToTime(totalMinutes) {
	var h = totalMinutes % 60;
	var m = totalMinutes - h * 60;

	retTime = h > 0 ? h.toString() + "h " : "" + m == NaN ? "0" : m.toString() + "m";

	return retTime;
}

class CourseScreen extends Component {
	componentDidMount() {
		this.props.fetchCourses();
	}

	render() {
		const { navigate } = this.props.navigation;

		const RenderCourses = ({ item, index }) => {
			return (
				<>
					<TouchableWithoutFeedback onPress={() => navigate("Module", { courseId: item.id })}>
						<Card
							key={index}
							featuredTitle={item.title}
							featuredSubtitle={minToTime(item.total_time) + " remaining"}
							onPress={() => navigate("Module", { courseId: item.id })}
							image={require("../../assets/images/services/nurse01.jpg")}
						>
							<Text>{item.description}</Text>
						</Card>
					</TouchableWithoutFeedback>
				</>
			);
		};

		return (
			<ScrollView style={styles.container}>
				<Text style={styles.developmentModeText}>{this.props.courses.errMess}</Text>
				<Text style={styles.developmentModeText}>This is Course Overview Screen</Text>
				<FlatList
					data={this.props.courses.courses}
					renderItem={RenderCourses}
					keyExtractor={item => item.id.toString()}
				/>
			</ScrollView>
		);
	}
}

CourseScreen.navigationOptions = {
	title: "Courses",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	developmentModeText: {
		marginBottom: 20,
		color: "rgba(0,0,0,0.4)",
		fontSize: 14,
		lineHeight: 19,
		textAlign: "center"
	}
});

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(CourseScreen);
