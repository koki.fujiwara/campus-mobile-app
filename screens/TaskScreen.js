import React, { Component } from "react";
import { ScrollView, StyleSheet, View, Text, FlatList, TouchableNativeFeedback } from "react-native";
import { ListItem, Button } from "react-native-elements";
import Colors from "../constants/Colors";
import { SCHEDULES } from "../shared/schedules";

class TaskScreen extends Component {
	constructor(props) {
		super(props);
		this.state = {
			schedules: SCHEDULES
		};
	}

	render() {
		const { navigate } = this.props.navigation;

		RenderTasks = ({ item, index }) => {
			return (
				<View
					style={{
						flex: 1,
						flexDirection: "row",
						marginVertical: 5,
						marginHorizontal: 10,
						alignSelf: "stretch",
						justifyContent: "space-evenly"
					}}
				>
					<View
						style={{
							justifyContent: "center",
							alignItems: "center"
						}}
					>
						<Text style={styles.weekday}>{item.weekday}</Text>
						<View style={item.status == "late" ? styles.dayboxLate : styles.dayboxUpcoming}>
							<Text style={item.status == "late" ? styles.dayLate : styles.dayUpcoming}>{item.day}</Text>
						</View>
					</View>
					<View style={item.status == "late" ? styles.taskBoxLate : styles.taskBoxUpcoming}>
						<TouchableNativeFeedback onPress={() => navigate(item.navigate)}>
							<Button
								key={index}
								title={item.task}
								subtitle={item.status}
								buttonStyle={{
									paddingVertical: 15,
									paddingHorizontal: 15,
									borderWidth: 0,
									alignSelf: "flex-start"
								}}
								titleStyle={item.status == "late" ? styles.taskTextLate : styles.taskTextUpcoming}
								// raised
								type="outline"
							/>
						</TouchableNativeFeedback>
					</View>
				</View>
			);
		};

		return (
			<>
				<ScrollView style={styles.container}>
					<FlatList data={this.state.schedules} renderItem={RenderTasks} keyExtractor={item => item.id.toString()} />
				</ScrollView>
			</>
		);
	}
}

TaskScreen.navigationOptions = {
	title: "Tasks",
	headerStyle: {
		backgroundColor: Colors.tintColor
	},
	headerTintColor: "#fff",
	headerTitleStyle: {
		color: "#fff"
	}
};

const styles = StyleSheet.create({
	container: {
		flex: 1,
		paddingTop: 15,
		backgroundColor: "#fff"
	},
	weekday: {
		fontSize: 10
	},
	dayboxUpcoming: {
		width: 40,
		height: 40,
		alignItems: "center",
		justifyContent: "center"
	},
	dayUpcoming: {
		fontSize: 24,
		color: "#000"
	},
	taskBoxUpcoming: {
		width: "80%",
		borderStyle: "solid",
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: Colors.tintColor
	},
	dayboxLate: {
		width: 40,
		height: 40,
		borderRadius: 20,
		alignItems: "center",
		justifyContent: "center",
		backgroundColor: "red"
	},
	taskTextUpcoming: {
		color: Colors.tintColor,
		alignSelf: "flex-start",
		marginLeft: 10
	},
	dayLate: {
		fontSize: 24,
		color: "#fff"
	},
	taskBoxLate: {
		width: "80%",
		borderStyle: "solid",
		borderRadius: 10,
		borderWidth: 0.5,
		borderColor: "#fff",
		backgroundColor: "red"
	},
	taskTextLate: {
		color: "#fff",
		alignSelf: "flex-start",
		marginLeft: 10
	}
});

export default TaskScreen;
