export const NOTIFICATONS = [
	{
		id: 0,
		username: "Koki",
		notificationtype: "Assignment",
		notificationtime: "18:21",
		notificationdate: "2019-08-27",
		profilepic: "../assets/images/services/nurse01.jpg",
		important: true,
		read: false,
		navigate: "Course"
	},

	{
		id: 1,
		username: "Li",
		notificationtype: "Li liked your message",
		notificationtime: "14:21",
		notificationdate: "2019-08-27",
		profilepic: "../assets/images/services/nurse01.jpg",
		important: false,
		read: false,
		navigate: "Chat"
	},

	{
		id: 2,
		username: "System",
		notificationtype: "Due Date Notification",
		notificationtime: "21:21",
		notificationdate: "2019-08-26",
		profilepic: "../assets/images/services/nurse01.jpg",
		important: false,
		read: true,
		navigate: "Course"
	},

	{
		id: 3,
		username: "System",
		notificationtype: "Passed Due Date",
		notificationtime: "21:21",
		notificationdate: "2019-08-26",
		profilepic: "..assetsimageslogosexa-logo.png",
		important: true,
		read: true,
		navigate: "Task"
	},

	{
		id: 4,
		username: "Zhang",
		notificationtype: "Commented On Your Message",
		notificationtime: "16:20",
		notificationdate: "2019-08-26",
		profilepic: "../assets/images/services/nurse01.jpg",
		important: false,
		read: true,
		navigate: "Chat"
	}
];
