export const ITEMS = [
	{
		moduleId: 0,
		items: [
			{
				id: 0,
				title: "手卫生技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 1,
				moduleId: 0,
				title: "无菌技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 2,
				moduleId: 0,
				title: "生命体正监测技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 3,
				moduleId: 0,
				title: "口腔护理技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 4,
				moduleId: 0,
				title: "作业1:基本护理技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 5,
				moduleId: 0,
				title: "留置导尿管技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 6,
				moduleId: 0,
				title: "留置导尿管技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 7,
				moduleId: 0,
				title: "作业2:留置导尿管技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 8,
				moduleId: 0,
				title: "大量不保留灌肠技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 9,
				moduleId: 0,
				title: "作业3:大量不保留灌肠技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 10,
				moduleId: 0,
				title: "氧气吸入（氧气筒供氧）技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 11,
				moduleId: 0,
				title: "氧气吸入（中心供氧）技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 12,
				moduleId: 0,
				title: "作业4:氧气吸入技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 13,
				moduleId: 0,
				title: "实践1:氧气吸入技术",
				itemType: "training",
				totalTime: "12 min",
				finished: true
			}
		]
	},
	{
		moduleId: 1,
		items: [
			{
				id: 0,
				title: "手卫生技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 1,
				moduleId: 0,
				title: "无菌技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 2,
				moduleId: 0,
				title: "生命体正监测技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 3,
				moduleId: 0,
				title: "口腔护理技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 4,
				moduleId: 0,
				title: "作业1:基本护理技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 5,
				moduleId: 0,
				title: "留置导尿管技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 6,
				moduleId: 0,
				title: "留置导尿管技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 7,
				moduleId: 0,
				title: "作业2:留置导尿管技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 8,
				moduleId: 0,
				title: "大量不保留灌肠技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 9,
				moduleId: 0,
				title: "作业3:大量不保留灌肠技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 10,
				moduleId: 0,
				title: "氧气吸入（氧气筒供氧）技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 11,
				moduleId: 0,
				title: "氧气吸入（中心供氧）技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 12,
				moduleId: 0,
				title: "作业4:氧气吸入技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 13,
				moduleId: 0,
				title: "实践1:氧气吸入技术",
				itemType: "training",
				totalTime: "12 min",
				finished: true
			}
		]
	},
	{
		moduleId: 2,
		items: [
			{
				id: 0,
				title: "手卫生技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 1,
				moduleId: 0,
				title: "无菌技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 2,
				moduleId: 0,
				title: "生命体正监测技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 3,
				moduleId: 0,
				title: "口腔护理技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 4,
				moduleId: 0,
				title: "作业1:基本护理技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 5,
				moduleId: 0,
				title: "留置导尿管技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 6,
				moduleId: 0,
				title: "留置导尿管技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 7,
				moduleId: 0,
				title: "作业2:留置导尿管技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 8,
				moduleId: 0,
				title: "大量不保留灌肠技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 9,
				moduleId: 0,
				title: "作业3:大量不保留灌肠技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 10,
				moduleId: 0,
				title: "氧气吸入（氧气筒供氧）技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 11,
				moduleId: 0,
				title: "氧气吸入（中心供氧）技术",
				itemType: "video",
				videoLink: "",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 12,
				moduleId: 0,
				title: "作业4:氧气吸入技术",
				itemType: "homeWork",
				totalTime: "12 min",
				finished: true
			},
			{
				id: 13,
				moduleId: 0,
				title: "实践1:氧气吸入技术",
				itemType: "training",
				totalTime: "12 min",
				finished: true
			}
		]
	}
];
