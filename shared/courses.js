export const COURSES = [
	{
		id: 0,
		title: "护理操作技术",
		image: "../assets/images/services/nurse01.jpg",
		description: "Please take a look at our newest course",
		categories: [""],
		totalTime: 12387,
		remainTime: 123,
		createdBy: "Koki Fujiwara",
		createdAt: "",
		active: true
	},
	{
		id: 1,
		title: "医疗操作技术",
		image: "../assets/images/services/medical01.jpg",
		description: "Please take a look at our newest course",
		categories: [""],
		totalTime: 8462,
		createdBy: "Koki Fujiwara",
		createdAt: "",
		active: true
	},
	{
		id: 2,
		title: "看护操作技术",
		image: "../assets/images/services/nurse02.jpg",
		description: "Please take a look at our newest course",
		categories: [""],
		totalTime: 8462,
		remainTime: 3400,
		createdBy: "Koki Fujiwara",
		createdAt: "",
		active: true,
		featured: true
	},
	{
		id: 3,
		title: "自立支援基础班",
		image: "../assets/images/services/medical01.jpg",
		description: "This is an awesome course about Japanese care tech",
		categories: [""],
		totalTime: 8462,
		createdBy: "Koki Fujiwara",
		createdAt: "",
		active: true
	},
	{
		id: 4,
		title: "自立支援高级版",
		image: "../assets/images/services/nurse02.jpg",
		description: "Please take a look at our newest course",
		categories: [""],
		totalTime: 8462,
		remainTime: 3400,
		createdBy: "Koki Fujiwara",
		createdAt: "",
		active: true,
		featured: true
	}
];
