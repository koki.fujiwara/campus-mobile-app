export const SCHEDULES = [
	{
		id: "0",
		date: "2019-08-9",
		month: "August",
		day: "9",
		weekday: "FRI",
		task: "Home work for MODULE 1 is due",
		status: "late",
		navigate: "Course"
	},
	{
		id: "1",
		date: "2019-08-15",
		month: "August",
		day: "15",
		weekday: "THU",
		task: "MODULE 1 completion",
		status: "upcoming",
		navigate: "Course"
	},
	{
		id: "2",
		date: "2019-08-19",
		month: "August",
		day: "19",
		weekday: "MON",
		task: "MODULE 2 enrollment",
		status: "upcoming",
		navigate: "Course"
	},
	{
		id: "3",
		date: "2019-08-23",
		month: "August",
		day: "23",
		weekday: "FRI",
		task: "Home work for MODULE 2 is due",
		status: "upcoming",
		navigate: "Chat"
	}
];
