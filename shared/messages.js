export const MESSAGES = [
	{
		chatRoomId: 0,
		username: "大灰狼",
		numUnread: 3,
		lastMessage: "你给我小心着点儿",
		content: [
			{
				messageId: 0,
				isUser: false,
				message: "你是不是想泡我女朋友啊",
				numLikes: 0,
				timestamp: 1268288900000
			},
			{
				messageId: 1,
				isUser: true,
				message: "你是谁？",
				numLikes: 1,
				timestamp: 1268289900000
			},
			{
				messageId: 2,
				isUser: false,
				message: "别装，我知道你老骚扰她",
				numLikes: 0,
				timestamp: 1268290000000
			},
			{
				messageId: 3,
				isUser: false,
				message: "小绵羊你知不知道？",
				numLikes: 0,
				timestamp: 1268290100000
			},
			{
				messageId: 4,
				isUser: false,
				message: "你给我小心着点儿",
				numLikes: 0,
				timestamp: 1268290100000
			}
		]
	},
	{
		chatRoomId: 1,
		username: "弘毅",
		numUnread: 1,
		lastMessage: "👌",
		content: [
			{
				messageId: 5,
				isUser: false,
				message: "你好，我想问一下第一个作业时什么时候交？",
				numLikes: 0,
				timestamp: 1267315200000
			},
			{
				messageId: 6,
				isUser: true,
				message: "好像是14号，我不确定我在帮你问一下",
				numLikes: 1,
				timestamp: 1267315300000
			},
			{
				messageId: 7,
				isUser: false,
				message: "好的，太谢谢了！",
				numLikes: 0,
				timestamp: 1267315400000
			},
			{
				messageId: 8,
				isUser: true,
				message: "不客气😊",
				numLikes: 0,
				timestamp: 1267315600000
			},
			{
				messageId: 9,
				isUser: false,
				message: "👌",
				numLikes: 0,
				timestamp: 1267316100000
			}
		]
	},
	{
		chatRoomId: 2,
		username: "小绵羊",
		lastMessage: "我今天看见你了😊",
		content: [
			{
				messageId: 10,
				isUser: true,
				message: "你好",
				numLikes: 0,
				timestamp: 1267217600000
			},
			{
				messageId: 11,
				isUser: true,
				message: "你干嘛呢",
				numLikes: 0,
				timestamp: 1267288800000
			},
			{
				messageId: 12,
				isUser: true,
				message: "我今天看见你了😊",
				numLikes: 0,
				timestamp: 1268288800000
			}
		]
	},
	{
		chatRoomId: 3,
		username: "东方不败",
		lastMessage: "原来如此",
		content: [
			{
				messageId: 10,
				isUser: true,
				message: "原来如此",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 4,
		username: "孙婉莹",
		lastMessage: "有点意思😊",
		content: [
			{
				messageId: 10,
				isUser: true,
				message: "有点意思😊",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 5,
		username: "刘晨",
		lastMessage: "那有点不好吧。。",
		content: [
			{
				messageId: 10,
				isUser: true,
				message: "那有点不好吧。。",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 6,
		username: "王磊",
		lastMessage: "还是这个好",
		content: [
			{
				messageId: 10,
				isUser: true,
				message: "还是这个好",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 7,
		username: "陈新",
		lastMessage: "你讨不讨厌",
		content: [
			{
				messageId: 10,
				isUser: false,
				message: "你讨不讨厌",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 8,
		username: "赵瑾琛",
		lastMessage: "哈哈",
		content: [
			{
				messageId: 10,
				isUser: false,
				message: "哈哈",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 8,
		username: "初升的太阳",
		lastMessage: "还行吧",
		content: [
			{
				messageId: 10,
				isUser: false,
				message: "还行吧",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 9,
		username: "雏鹰起飞",
		lastMessage: "跳一跳",
		content: [
			{
				messageId: 10,
				isUser: false,
				message: "跳一跳",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	},
	{
		chatRoomId: 10,
		username: "非主流",
		lastMessage: "...",
		content: [
			{
				messageId: 10,
				isUser: false,
				message: "...",
				numLikes: 0,
				timestamp: 1267217600000
			}
		]
	}
];
