# weCoach App

## Run in local

First, set up weCoach-api project, and run it locally. Make sure http://wecoach-api.local.exwzd.com is available.

Connect your mobile phone to the same network of your PC. Install Expo Client on mobile.
Get the IP of your PC:

```
ipconfig getifaddr en0
```

and set this IP as the DNS in your mobile. (This is for accessing http://wecoach-api.local.exwzd.com from the mobile. Better to access the URL in mobile browser to verify it)

Run

```
./start.sh
```

Open http://localhost:19002, select the "LAN" connection, scan the QR code in Expo client.

If using expo build

### Android

![](img/local-android-qr.png)

### iOS

![](img/local-ios-qr.png)

To stop, run

```
./stop.sh
```

Use `./start.sh` to reload code changes as well.

## Deploy to Dev

Merge code to master branch and push. Build [Jenkins job](https://jenkins.corp.exwzd.com/job/exachina/job/wecoach-app/). Verify http://wecoach-app.dev.exwzd.com/android-index.json and http://wecoach-app.dev.exwzd.com/ios-index.json available. Also verify http://wecoach-api.dev.exwzd.com/ available from mobile.

Use Expo client to scan QR code to open the applicaiton:

### Android

![](img/android-qr.png)

### iOS

![](img/ios-qr.png)
