import React from "react";
import { createStackNavigator, createDrawerNavigator, Easing } from "react-navigation";

import CourseScreen from "../screens/courses/CourseScreen";
import ChatScreen from "../screens/ChatScreen";
import NotificationScreen from "../screens/NotificationScreen";
import CalendarScreen from "../screens/CalendarScreen";
import ModuleScreen from "../screens/courses/ModuleScreen";
import TaskScreen from "../screens/TaskScreen";

import chatNavigator from "./ChatNavigator";
import courseNavigator from "./CourseNavigator";
import settingsNavigator from "./SettingNavigator";

const transitionConfig = () => {
	return {
		transitionSpec: {
			duration: 350,
			useNativeDriver: true
		},
		screenInterpolator: sceneProps => {
			const { layout, position, scene } = sceneProps;

			const thisSceneIndex = scene.index;
			const width = layout.initWidth;

			const translateX = position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex],
				outputRange: [width, 0]
			});

			return { transform: [{ translateX }] };
		}
	};
};

const notificationNavigator = createStackNavigator(
	{
		Notification: { screen: NotificationScreen }
		// Module: { screen: ModuleScreen },
		// Chat: { screen: ChatScreen },
		// Task: { screen: TaskScreen },
		// Course: { screen: CourseScreen }
	},

	{
		initialRouteName: "Notification",
		transitionConfig
	}
);

export default notificationNavigator;
