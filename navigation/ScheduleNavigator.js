import React from "react";
import {
	TabNavigator,
	createMaterialTopTabNavigator,
	createStackNavigator,
	createDrawerNavigator,
	Easing
} from "react-navigation";
import { Platform, StatusBar } from "react-native";
import Colors from "../constants/Colors";
import CalendarScreen from "../screens/CalendarScreen";
import TaskScreen from "../screens/TaskScreen";
import ModuleScreen from "../screens/courses/ModuleScreen";
import CourseScreen from "../screens/courses/CourseScreen";

const transitionConfig = () => {
	return {
		transitionSpec: {
			duration: 350,
			useNativeDriver: true
		},
		screenInterpolator: sceneProps => {
			const { layout, position, scene } = sceneProps;

			const thisSceneIndex = scene.index;
			const width = layout.initWidth;

			const translateX = position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex],
				outputRange: [width, 0]
			});

			return { transform: [{ translateX }] };
		}
	};
};

scheduleTabNavigator = createMaterialTopTabNavigator(
	{
		Task: { screen: TaskScreen },
		Calendar: { screen: CalendarScreen }
	},
	{
		initialRouteName: "Task",
		swipeEnabled: true,
		tabBarOptions: {
			activeTintColor: "#fff",
			inactiveTintColor: "#fff",
			showIcon: false,
			indicatorStyle: {
				borderBottomColor: "#ffffff",
				borderBottomWidth: 2
			},
			labelStyle: {
				// fontSize: 12,
				justifyContent: "center",
				alignItems: "center"
			},
			style: {
				backgroundColor: Colors.tintColor
			},
			tabStyle: {
				paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight,
				justifyContent: "center",
				alignItems: "center"
			}
		}
	}
);

scheduleNavigator = createStackNavigator(
	{
		scheduleTabNavigator,
		Module: { screen: ModuleScreen },
		Course: { screen: CourseScreen }
	},
	{
		headerMode: "none",
		navigationOptions: {
			headerVisible: false
		},
		initialRouteName: "scheduleTabNavigator",
		transitionConfig
	}
);

export default scheduleNavigator;
