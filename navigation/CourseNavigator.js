import React from "react";
import { createStackNavigator, createDrawerNavigator, Easing } from "react-navigation";

import CourseScreen from "../screens/courses/CourseScreen";
import ModuleScreen from "../screens/courses/ModuleScreen";
import ItemScreen from "../screens/courses/ItemScreen";
import VideoScreen from "../screens/courses/VideoScreen";
import TrainingScreen from "../screens/courses/TrainingScreen";

const transitionConfig = () => {
	return {
		transitionSpec: {
			duration: 350,
			useNativeDriver: true
		},
		screenInterpolator: sceneProps => {
			const { layout, position, scene } = sceneProps;

			const thisSceneIndex = scene.index;
			const width = layout.initWidth;

			const translateX = position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex],
				outputRange: [width, 0]
			});

			return { transform: [{ translateX }] };
		}
	};
};

const courseNavigator = createStackNavigator(
	{
		Course: { screen: CourseScreen },
		Module: { screen: ModuleScreen },
		Item: { screen: ItemScreen },
		Video: { screen: VideoScreen },
		Training: { screen: TrainingScreen }
	},
	{
		initialRouteName: "Course",
		transitionConfig
	}
);

export default courseNavigator;
