import React from "react";
import { Platform } from "react-native";
import { createStackNavigator, createBottomTabNavigator } from "react-navigation";

import TabBarIcon from "../components/TabBarIcon";
import userNavigator from "./ChatNavigator";
import courseNavigator from "./CourseNavigator";
import settingsNavigator from "./SettingNavigator";
import notificationNavigator from "./NotificationNavigator";
// import ScheduleScreen from "../screens/ScheduleScreen";
import scheduleNavigator from "./ScheduleNavigator";

const config = Platform.select({
	web: { headerMode: "screen" },
	default: {}
});

const NotificationStack = notificationNavigator;

NotificationStack.navigationOptions = {
	tabBarLabel: "Notification",
	tabBarIcon: ({ focused }) => (
		<TabBarIcon
			focused={focused}
			name={Platform.OS === "ios" ? `ios-notifications-outline` : "md-notifications-outline"}
		/>
	)
};

const ChatStack = userNavigator;

ChatStack.navigationOptions = {
	tabBarLabel: "Chat",
	tabBarIcon: ({ focused }) => (
		<TabBarIcon focused={focused} name={Platform.OS === "ios" ? `ios-chatboxes` : "md-chatboxes"} />
	)
};

// Course Navigator
const CourseStack = courseNavigator;

CourseStack.navigationOptions = {
	tabBarLabel: "Courses",
	tabBarIcon: ({ focused }) => <TabBarIcon focused={focused} name={Platform.OS === "ios" ? `ios-book` : "md-book"} />
};

// Calendar

const ScheduleStack = scheduleNavigator;

ScheduleStack.navigationOptions = {
	tabBarLabel: "Schedule",
	tabBarIcon: ({ focused }) => (
		<TabBarIcon focused={focused} name={Platform.OS === "ios" ? `ios-calendar` : "md-calendar"} />
	)
};

const SettingsStack = settingsNavigator;

SettingsStack.navigationOptions = {
	tabBarLabel: "Settings",
	tabBarIcon: ({ focused }) => (
		<TabBarIcon focused={focused} name={Platform.OS === "ios" ? "ios-options" : "md-options"} />
	)
};

const tabNavigator = createBottomTabNavigator(
	{
		NotificationStack,
		ChatStack,
		CourseStack,
		ScheduleStack,
		SettingsStack
	},
	{ initialRouteName: "CourseStack" }
);

tabNavigator.path = "";

export default tabNavigator;
