import React from "react";
import { createStackNavigator, createDrawerNavigator, Easing } from "react-navigation";

import SettingsScreen from "../screens/SettingsScreen";
import AccountInfoScreen from "../screens/AccountInfoScreen";
import AppInfoScreen from "../screens/AppInfoScreen";

const transitionConfig = () => {
	return {
		transitionSpec: {
			duration: 350,
			useNativeDriver: true
		},
		screenInterpolator: sceneProps => {
			const { layout, position, scene } = sceneProps;

			const thisSceneIndex = scene.index;
			const width = layout.initWidth;

			const translateX = position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex],
				outputRange: [width, 0]
			});

			return { transform: [{ translateX }] };
		}
	};
};

const settingsNavigator = createStackNavigator(
	{
		Settings: { screen: SettingsScreen },
		AccountInfo: { screen: AccountInfoScreen },
		AppInfo: { screen: AppInfoScreen }
	},
	{
		initialRouteName: "Settings",
		transitionConfig
	}
);

export default settingsNavigator;
