import React from "react";
import { createStackNavigator, createMaterialTopTabNavigator, Easing } from "react-navigation";

import ChatScreen from "../screens/ChatScreen";
import ChatRoomScreen from "../screens/ChatRoomScreen";

import GroupScreen from "../screens/GroupScreen";

import { Platform, StatusBar } from "react-native";
import Colors from "../constants/Colors";

const transitionConfig = () => {
	return {
		transitionSpec: {
			duration: 350,
			useNativeDriver: true
		},
		screenInterpolator: sceneProps => {
			const { layout, position, scene } = sceneProps;

			const thisSceneIndex = scene.index;
			const width = layout.initWidth;

			const translateX = position.interpolate({
				inputRange: [thisSceneIndex - 1, thisSceneIndex],
				outputRange: [width, 0]
			});

			return { transform: [{ translateX }] };
		}
	};
};

const chatNavigator = createStackNavigator(
	{
		Chat: { screen: ChatScreen },
		ChatRoom: { screen: ChatRoomScreen }
	},
	{
		initialRouteName: "Chat",
		transitionConfig
	}
);

const userNavigator = createMaterialTopTabNavigator(
	{
		Chat: chatNavigator,
		Group: { screen: GroupScreen }
	},
	{
		initialRouteName: "Chat",
		swipeEnabled: true,
		tabBarOptions: {
			activeTintColor: "#fff",
			inactiveTintColor: "#fff",
			showIcon: false,
			indicatorStyle: {
				borderBottomColor: "#ffffff",
				borderBottomWidth: 2
			},
			labelStyle: {
				// fontSize: 12,
				justifyContent: "center",
				alignItems: "center"
			},
			style: {
				backgroundColor: Colors.tintColor
			},
			tabStyle: {
				paddingTop: Platform.OS === "ios" ? 0 : StatusBar.currentHeight,
				justifyContent: "center",
				alignItems: "center"
			}
		}
	}
);

export default userNavigator;
