FROM node:10
ARG env
WORKDIR /usr/src/app
RUN wget -q https://github.com/m3ng9i/ran/releases/download/v0.1.4/ran_linux_amd64.zip -O /tmp/ran_linux_amd64.zip && \
    unzip /tmp/ran_linux_amd64.zip -d /tmp && mv /tmp/ran_linux_amd64 /usr/local/bin/ran && \
    rm /tmp/ran_linux_amd64.zip
RUN npm install create-react-native-app -g
RUN npm install expo-cli -g
COPY package.json package.json
COPY package-lock.json package-lock.json

RUN npm install

COPY . .
RUN cp envs/$env/axios.js ./ && cp envs/$env/baseUrl.js ./shared/
RUN expo export --public-url http://wecoach-app.$env.exwzd.com --dev

ENTRYPOINT [ "ran" ]
CMD ["-r", "/usr/src/app/dist"]
