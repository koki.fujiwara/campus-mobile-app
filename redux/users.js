import * as ActionTypes from "./ActionTypes";

export const users = (
	state = {
		isLoading: true,
		errMess: null,
		users: [],
		moreUsers: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.ADD_USERS:
			return { ...state, isLoading: false, errMess: null, users: action.payload, moreUsers: [] };

		case ActionTypes.FILTER_USERS:
			return { ...state, isLoading: false, errMess: null, moreUsers: action.payload };

		case ActionTypes.USERS_LOADING:
			return { ...state, isLoading: true, errMess: null, users: [], moreUsers: [] };

		case ActionTypes.USERS_FAILED:
			return { ...state, isLoading: false, errMess: action.payload };

		default:
			return state;
	}
};
