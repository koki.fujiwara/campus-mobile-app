import * as ActionTypes from "./ActionTypes";

export const modules = (
	state = {
		isLoading: true,
		errMess: null,
		module: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.ADD_MODULE:
			return { ...status, isLoading: false, errMess: null, modules: action.payload };

		case ActionTypes.MODULE_LOADING:
			return { ...state, isLoading: true, errMess: null, modules: [] };

		case ActionTypes.MODULE_FAILED:
			return { ...state, isLoading: false, errMess: action.payload };

		default:
			return state;
	}
};
