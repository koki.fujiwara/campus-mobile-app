import * as ActionTypes from "./ActionTypes";
import { baseUrl } from "../shared/baseUrl";
import io from "socket.io-client";
// import API from "../axios";

export const fetchCourses = () => dispatch => {
	dispatch(coursesLoading());

	return fetch(baseUrl + "courses")
		.then(
			response => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error("Error " + response.status + ": " + response.statusText);
					error.response = response;
					throw error;
				}
			},
			error => {
				var errMess = new Error(error.message);
				console.log(error);
				throw errMess;
			}
		)
		.then(response => response.json())
		.then(courses => {
			dispatch(addCourses(courses));
		})
		.catch(error => dispatch(coursesFailed(error.message)));
};

export const coursesLoading = () => ({
	type: ActionTypes.COURSES_LOADING
});

export const coursesFailed = errMess => ({
	type: ActionTypes.COURSES_FAILED,
	payload: errMess
});

export const addCourses = courses => ({
	type: ActionTypes.ADD_COURSES,
	payload: courses
});

export const fetchSelectedCourse = id => dispatch => {
	dispatch(courseLoading());

	return fetch(baseUrl + "courses/" + id + "/modules")
		.then(
			response => {
				if (response.ok) {
					// console.log(response);
					return response;
				} else {
					var error = new Error("Error " + response.status + ": " + response.statusText);
					error.response = response;
					throw error;
				}
			},
			error => {
				var errMess = new Error(error.message);
				throw errMess;
			}
		)
		.then(response => response.json())
		.then(course => {
			dispatch(addCourse(course));
		})
		.catch(error => dispatch(courseFailed(error.message)));
};

export const courseLoading = () => ({
	type: ActionTypes.COURSE_LOADING
});

export const courseFailed = errMess => ({
	type: ActionTypes.COURSE_FAILED,
	payload: errMess
});

export const addCourse = course => ({
	type: ActionTypes.ADD_COURSE,
	payload: course
});

export const fetchItems = moduleId => dispatch => {
	dispatch(itemsLoading());

	return fetch(baseUrl + "modules/" + moduleId + "/items")
		.then(
			response => {
				if (response.ok) {
					return response;
				} else {
					var error = new Error("Error " + response.status + ": " + response.statusText);
					error.response = response;
					throw error;
				}
			},
			error => {
				var errMess = new Error(error.message);
				throw errMess;
			}
		)
		.then(response => response.json())
		.then(items => {
			dispatch(addItems(items));
		})
		.catch(error => dispatch(itemsFailed(error.message)));
};

export const itemsLoading = () => ({
	type: ActionTypes.ITEMS_LOADING
});

export const itemsFailed = errMess => ({
	type: ActionTypes.ITEMS_FAILED,
	payload: errMess
});

export const addItems = items => ({
	type: ActionTypes.ADD_ITEMS,
	payload: items
});

export const fetchUsers = userOffset => dispatch => {
	dispatch(usersLoading());

	return fetch(baseUrl + "users/?page=" + userOffset)
		.then(response => {
			if (response.ok) {
				return response;
			} else {
				var error = new Error(error.message);
				throw error;
			}
		})
		.then(response => response.json())
		.then(users => {
			dispatch(addUsers(users));
		})
		.catch(error => dispatch(usersFailed(error.message)));
};

export const fetchMoreUsers = userOffset => dispatch => {
	dispatch(userLoading());

	return fetch(baseUrl + "users/fetch/?page=" + userOffset)
		.then(response => {
			if (response.ok) {
				return response;
			} else {
				var error = new Error(error.message);
				throw error;
			}
		})
		.then(response => response.json())
		.then(users => dispatch(moreUsers(users)))
		.catch(error => dispatch(userFailed(error)));
};

export const fetchFilterUsers = (users, keywords, userOffset) => dispatch => {
	dispatch(usersLoading());

	if (keywords.length === 0) {
		dispatch(fetchUsers(userOffset));
	} else {
		try {
			dispatch(
				filterUsers(
					users.filter(
						user =>
							user.first_name.includes(keywords) ||
							user.last_name.includes(keywords) ||
							user.username.includes(keywords)
					)
				)
			);
		} catch (e) {
			dispatch(usersFailed(e));
		}
	}
};

export const usersLoading = () => ({
	type: ActionTypes.USERS_LOADING
});

export const usersFailed = errMess => ({
	type: ActionTypes.USERS_FAILED,
	payload: errMess
});

export const addUsers = users => ({
	type: ActionTypes.ADD_USERS,
	payload: users
});

export const filterUsers = users => ({
	type: ActionTypes.FILTER_USERS,
	payload: users
});

export const userLogin = login_info => dispatch => {
	dispatch(loginLoading);

	// console.log(login_info);

	try {
		dispatch(login(login_info));
	} catch (e) {
		dispatch(loginFailed(e));
	}
};

export const loginLoading = () => ({
	type: ActionTypes.LOGIN_LOADING
});

export const loginFailed = errMess => ({
	type: ActionTypes.LOGIN_FAILED,
	payload: errMess
});

export const login = login_info => ({
	type: ActionTypes.LOGIN,
	payload: login_info
});

export const logout = () => dispatch => {
	dispatch({
		type: ActionTypes.LOGOUT
	});
};

export const connectSocket = (socket, status) => dispatch => {
	// dispatch(socketConnecting());

	socket.on("connect", function() {
		console.log("connected");
		dispatch(socketConnected());
		dispatch(listenToSocket(socket, status));
	});

	socket.on("error", function(e) {
		console.log("Error: " + e);
	});
};

export const reconnectSocket = socket => dispatch => {
	status = dispatch({ type: ActionTypes.SOCKET_RECONNECT });
	dispatch(connectSocket(socket, status));
};

export const listenToSocket = (socket, status) => dispatch => {
	socket.on("id", function(id) {
		console.log("id: " + id);
		status = { ...status, sessionId: id };
		console.log("Socket Status: " + status);
		dispatch(updateSocketStatus(status));
	});
	socket.on("message", function(msg) {
		console.log("message: " + msg);
		status = { ...status, message: msg };
		console.log("Socket Status: " + status);
		dispatch(updateSocketStatus(status));
	});
	socket.on("personal", function(msg) {
		console.log("personal: " + msg);
		status = { ...status, personal: msg };
		console.log("Socket Status: " + status);
		dispatch(updateSocketStatus(status));
	});
	socket.on("startSession", function() {
		status = { ...status, sessionStarted: true };
		console.log("RENDER CONTENT: " + JSON.stringify(status));
		dispatch(updateSocketStatus(status));
	});
};

export const updateSocketStatus = status => ({
	type: ActionTypes.SOCKET_UPDATE,
	payload: status
});

export const socketConnecting = () => ({
	type: ActionTypes.SOCKET_CONNECTING
});

export const socketConnected = status => ({
	type: ActionTypes.SOCKET_CONNECTED,
	payload: status
});

export const socketFailed = errMess => ({
	type: ActionTypes.SOCKET_FAILED,
	payload: errMess
});

export const socketDisconnected = dispatch => {
	dispatch({
		type: ActionTypes.SOCKET_DISCONNECT
	});
};
