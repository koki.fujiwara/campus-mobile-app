import * as ActionTypes from "./ActionTypes";

export const groups = (
	state = {
		isLoading: true,
		errMess: null,
		groups: []
	},
	action
) => {
	switch (action) {
		case ActionTypes.ADD_GROUPS:
			return { ...state, isLoading: false, errMess: null, groups: action.payload };

		case ActionTypes.CREATE_GROUP:
			return { ...state, isLoading: false, errMess: null, groups: action.payload };

		case ActionTypes.ADD_USERS_TO_GROUP:
			return { ...state, isLoading: false, errMess: null, groups: action.payload };

		case ActionTypes.GROUPS_LOADING:
			return { ...state, isLoading: true, errMess: null, groups: [] };

		case ActionTypes.GROUPS_FAILED:
			return { ...state, isLoading: false, errMess: action.payload };

		default:
			return state;
	}
};
