import * as ActionTypes from "./ActionTypes";

const initialState = {
	isConnecting: true,
	isConnected: false,
	errMess: null,
	status: {
		sessionId: null,
		sessionStarted: false,
		recordStarted: false
	},
	cameraStatus: null
};

export const sockets = (state = initialState, action) => {
	switch (action.type) {
		case ActionTypes.SOCKET_CONNECTING:
			return { ...state, isConnecting: true, isConnected: false, errMess: null, status: null };

		case ActionTypes.SOCKET_CONNECTED:
			return { ...state, isConnecting: false, isConnected: true, errMess: null, status: null };

		case ActionTypes.SOCKET_FAILED:
			return { ...state, isConnecting: false, isConnected: false, errMess: action.payload, status: null };

		case ActionTypes.SOCKET_DISCONNECT:
			return { ...state, isConnecting: false, isConnected: false, errMess: null, status: null };

		case ActionTypes.SOCKET_UPDATE:
			return { ...state, isConnecting: false, isConnected: true, errMess: null, status: action.payload };

		case ActionTypes.SOCKET_RECONNECT:
			return initialState;

		default:
			return state;
	}
};
