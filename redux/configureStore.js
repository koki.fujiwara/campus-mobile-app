import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";

import { courses, selectedCourse } from "./courses";
import { modules } from "./modules";
import { items } from "./items";
import { users } from "./users";
import { groups } from "./groups";
import { login } from "./login";
import { sockets } from "./sockets";

export const ConfigureStore = () => {
	const store = createStore(
		combineReducers({
			courses,
			selectedCourse,
			modules,
			items,
			users,
			login,
			sockets
		}),
		applyMiddleware(thunk, logger)
	);

	return store;
};
