import * as ActionTypes from "./ActionTypes";

export const login = (
	state = {
		isLoading: false,
		errMess: null,
		loggedIn: false,
		username: null,
		pass_hash: null
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.LOGIN:
			return {
				...state,
				isLoading: false,
				errMess: null,
				loggedIn: true,
				username: action.payload.username,
				pass_hash: action.payload.pass_hash
			};

		case ActionTypes.LOGIN_LOADING:
			return { ...state, isLoading: true, errMess: null, loggedIn: false };

		case ActionTypes.LOGOUT:
			return { ...state, isLoading: false, errMess: null, loggedIn: false };

		case ActionTypes.LOGIN_FAILED:
			return { ...state, isLoading: false, errMess: action.payload, loggedIn: false };

		default:
			return state;
	}
};
