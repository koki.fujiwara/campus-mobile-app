import * as ActionTypes from "./ActionTypes";

export const courses = (
	state = {
		isLoading: false,
		errMess: null,
		courses: []
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.ADD_COURSES:
			return { ...state, isLoading: false, errMess: null, courses: action.payload };

		case ActionTypes.COURSES_LOADING:
			return { ...state, isLoading: true, errMess: null, courses: [] };

		case ActionTypes.COURSES_FAILED:
			return { ...state, isLoading: false, errMess: action.payload };

		default:
			return state;
	}
};

export const selectedCourse = (
	state = {
		isLoading: false,
		errMess: null,
		course: null
	},
	action
) => {
	switch (action.type) {
		case ActionTypes.ADD_COURSE:
			return {
				...state,
				isLoading: false,
				errMess: null,
				course: action.payload
			};
		case ActionTypes.COURSE_LOADING:
			return { ...state, isLoading: true, errMess: null, course: null };

		case ActionTypes.COURSE_FAILED:
			return { ...state, isLoading: false, errMess: action.payload, course: null };

		default:
			return state;
	}
};
