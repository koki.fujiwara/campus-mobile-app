#!/bin/bash
set -e

IP="$(ipconfig getifaddr en0)" docker-compose up -d --build
