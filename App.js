import { AppLoading } from "expo";
import { Asset } from "expo-asset";
import * as Font from "expo-font";
import React, { useState, Component } from "react";
import { Platform, StatusBar, StyleSheet, View, AppRegistry } from "react-native";
import { Ionicons } from "@expo/vector-icons";
import { Provider } from "react-redux";
import { ConfigureStore } from "./redux/configureStore";

// import LoginScreen from "./screens/registry/LoginScreen";

// import AppNavigator from "./navigation/AppNavigator";
import MainScreen from "./screens/MainScreen";

const store = ConfigureStore();

class App extends Component {
	// constructor(props) {
	// 	super(props);
	// 	this.state = {
	// 		isLoggedIn: false
	// 	};
	// }
	componentWillMount = () => {
		loadResourcesAsync();
	};

	// const [isLoadingComplete, setLoadingComplete] = useState(false);

	render() {
		// const LoginRequired = () => {
		// 	if (this.state.isLoggedIn) {
		// 		return <AppNavigator onLogoutPress={() => this.setState({ isLoggedIn: false })} />;
		// 	} else {
		// 		return <LoginScreen onLoginPress={() => this.setState({ isLoggedIn: true })} />;
		// 	}
		// };

		// if (!isLoadingComplete && !props.skipLoadingScreen) {
		// 	return (
		// 		<AppLoading
		// 			startAsync={loadResourcesAsync}
		// 			onError={handleLoadingError}
		// 			onFinish={() => handleFinishLoading(setLoadingComplete)}
		// 		/>
		// 	);
		// } else {
		return (
			<Provider store={store}>
				<View style={styles.container}>
					{Platform.OS === "ios" && <StatusBar barStyle="default" />}
					<MainScreen />
				</View>
			</Provider>
		);
		// }
	}
}

async function loadResourcesAsync() {
	await Font.loadAsync({
		// This is the font that we are using for our tab bar
		...Ionicons.font,
		// We include SpaceMono because we use it in HomeScreen.js. Feel free to
		// remove this if you are not using it in your app
		"space-mono": require("./assets/fonts/SpaceMono-Regular.ttf")
	});
}

function handleLoadingError(error) {
	// In this case, you might want to report the error to your error reporting
	// service, for example Sentry
	console.warn(error);
}

function handleFinishLoading(setLoadingComplete) {
	setLoadingComplete(true);
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		backgroundColor: "#fff"
	}
});

// AppRegistry.registerComponent(App, () => App);

export default App;
