const tintColor = "#2f95dc";
const accentColor = "#ff6a6b";

export default {
	tintColor,
	accentColor,
	exaBlue: "#1400c8",
	tabIconDefault: "#ccc",
	tabIconSelected: tintColor,
	tabBar: "#fefefe",
	errorBackground: "red",
	errorText: "#fff",
	warningBackground: "#EAEB5E",
	warningText: "#666804",
	noticeBackground: tintColor,
	noticeText: "#fff"
};
