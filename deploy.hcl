job "wecoach-app" {
  datacenters = ["ap-northeast-1a"]
  meta {
    department = "exachina"
    product = "wecoach"
    component = "wecoach-app"
  }
  group "wecoach-app" {
    count = 1
    restart {
      attempts = 3
      delay    = "30s"
    }

    task "app" {
      driver = "docker"
      config {
        image        = "410319912229.dkr.ecr.ap-northeast-1.amazonaws.com/exachina/wecoach_app:latest"
        network_mode = "bridge"
        port_map = {
          http = "8080"
        }
      }
      resources {
        cpu    = 100 # MHz
        memory = 100 # MB

        network {
          port "http" {}
        }
      }

      service {
        name = "wecoach-app"
        tags = ["urlprefix-wecoach-app.${meta.environment}.exwzd.com/"]
        port = "http"

        check {
          name     = "wecoach-app-check"
          type     = "http"
          port     = "http"
          path     = "/ios-index.json"
          interval = "5s"
          timeout  = "2s"

          check_restart {
            limit           = 3
            grace           = "90s"
            ignore_warnings = false
          }
        }
      }
    }

  }
}
