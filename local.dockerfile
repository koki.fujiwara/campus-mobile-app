FROM node:10
WORKDIR /usr/src/app

RUN npm install create-react-native-app -g
RUN npm install add expo-cli -g
COPY package.json package.json
# COPY package-lock.json package-lock.json

RUN npm install

COPY . .

# RUN cp envs/dev/axios.js ./
# RUN cp envs/dev/baseUrl.js ./shared/

CMD ["yarn", "start"]